var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/public-tchat";

MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("public-tchat");
    console.log("BDD Crée")

    dbo.createCollection("message", function(err, res) {
        if (err) throw err;
        console.log("Collection created!");
        db.close();
    });
});