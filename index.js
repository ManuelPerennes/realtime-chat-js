const express = require('express')
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
let users = [];
const PORT = 3005
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/public-tchat";
const moment = require('moment');

// On sert le dossier plublic/
app.use(express.static('public'));

// On se sert de socket IO pour emettre / diffuser des événéments
io.on('connection', (socket) => {

    // On garde l'id du user
    const userId = socket.id

    // Ici: renvoyer l'historique stocké dans mongo.
    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("public-tchat");

        dbo.collection("message").find({}).toArray(function(err, result) {
            if (err) throw err;

            io.emit('connection', result);

            db.close();
        });
    });

    // Sur l'action d'une nouvelle connextion d'un utilisateur
    socket.on('newUser', (username) => {

        // On le rajoute à la liste des utiliseurs
        users.push({ id: userId, username: username });

        // On prévient tout le monde de son arrivé
        io.emit('newNotification', username + ' vient de se connecter');

        // On demande à tous le monde mettre à jour la liste des utilisateurs
        io.emit('updateUsersList', users);
    });

    // Sur l'action d'un nouveau message reçu
    socket.on('newMessage', (username, msg, date) => {
        // Ici: stocker dans mongo ? 
        date = new Date()
            // Ici on stock les messages 
        MongoClient.connect(url, function(err, db) {
            if (err) throw err;
            var dbo = db.db("public-tchat");
            var myobj = { username: username, msg: msg, date: date };
            dbo.collection("message").insertOne(myobj, function(err, res) {
                if (err) throw err;
                console.log("1 message stocké !");
                // On diffuse le message à tout le monde
                io.emit('newMessage', username, msg, date);
                db.close();
            });
        });
    });

    // Sur l'action d'une déconnextion d'un utilisateur
    socket.on('disconnect', () => {

        // On cherche le user dans la liste pour le retirer
        users = users.filter(user => {
            return user.id != userId;
        });

        // On demande à tous le monde mettre à jour la liste des utilisateurs
        io.emit('updateUsersList', users);
    });
});

http.listen(PORT, () => {
    console.log('listening on http://localhost:' + PORT);
});