const socket = io()

let username = localStorage.getItem('pseudo')
const messages = document.getElementById('messages')
const form = document.getElementById('form')
const input = document.getElementById('input')
const usersHtml = document.getElementById('users')



// Demande du pseudo utilisateur (si pas dans le localstorage)
while (!username) {
    username = prompt('Quel est votre pseudo')
    localStorage.setItem('pseudo', username)
}
if (username === 'pseudo')
    prompt('pseudo déjà use')

// Envoi du nouveau user
socket.emit('newUser', username)

// Envoi d'un message
form.addEventListener('submit', function(e) {
    e.preventDefault()
    if (input.value) {
        const msg = input.value;
        const date = new Date();
        socket.emit('newMessage', username, msg, date)
        input.value = ''
    }
})

// Récupération des nouveaux messages
socket.on('newMessage', function(username, msg, date) {
    var item = document.createElement('li')
    item.textContent = username + ': ' + msg + ' - ' + moment(date).calendar();
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération des nouvelles notification
socket.on('newNotification', function(msg) {
    var item = document.createElement('li')
    item.classList = 'notif'
    item.textContent = msg
    messages.appendChild(item)
    window.scrollTo(0, document.body.scrollHeight)
})

// Récupération de la liste des utilisateurs
socket.on('updateUsersList', function(users) {
    // Mettre à jour la liste dans le DOM (document) => Page html
    console.log(users, 'Users list');

    //VIDERUSERLIST
    usersHtml.textContent = '';
    users.forEach(user => {
        const li = document.createElement('li')
        li.textContent = user.username
        usersHtml.appendChild(li)
    })
})

socket.on('connection', function(result) {
    messages.textContent = "";
    console.log(result)

    result.forEach(Message => {
        let item = document.createElement('li');
        item.textContent = Message.username + ' : ' + Message.msg + ' ' + moment(Message.date).startOf('hour').fromNow();
        messages.appendChild(item);
    })
});


// historique des messages ;